﻿using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FortCode.Security
{
    public class FortCodeUserManager : UserManager<FortCodeUserIdentity>
    {
        //public AppUserManager(AppUserStore a_store)
        //    : base(a_store)
        //{
        //    _container = a_container;
        //    _emailService = _container.GetInstance<IEmailService>();

        //    PasswordHasher = new PasswordHasher();
        //}

        public FortCodeUserManager(IUserStore<FortCodeUserIdentity> store, 
            IOptions<IdentityOptions> optionsAccessor, 
            IPasswordHasher<FortCodeUserIdentity> passwordHasher, 
            IEnumerable<IUserValidator<FortCodeUserIdentity>> userValidators, 
            IEnumerable<IPasswordValidator<FortCodeUserIdentity>> passwordValidators, 
            ILookupNormalizer keyNormalizer, 
            IdentityErrorDescriber errors, 
            IServiceProvider services, 
            ILogger<UserManager<FortCodeUserIdentity>> logger) : base(store, optionsAccessor, passwordHasher, userValidators, passwordValidators, keyNormalizer, errors, services, logger)
        {
            //if (store == null)
            //{
            //    throw new ArgumentNullException("store");
            //}

            //Store = store;
            //Options = (optionsAccessor?.Value ?? new IdentityOptions());
            //PasswordHasher = passwordHasher;
            //KeyNormalizer = keyNormalizer;
            //ErrorDescriber = errors;
            //Logger = logger;
            //if (userValidators != null)
            //{
            //    foreach (IUserValidator<TUser> userValidator in userValidators)
            //    {
            //        UserValidators.Add(userValidator);
            //    }
            //}

            //if (passwordValidators != null)
            //{
            //    foreach (IPasswordValidator<TUser> passwordValidator in passwordValidators)
            //    {
            //        PasswordValidators.Add(passwordValidator);
            //    }
            //}

            //_services = services;
            //if (services != null)
            //{
            //    foreach (string key in Options.Tokens.ProviderMap.Keys)
            //    {
            //        TokenProviderDescriptor tokenProviderDescriptor = Options.Tokens.ProviderMap[key];
            //        IUserTwoFactorTokenProvider<TUser> userTwoFactorTokenProvider = (tokenProviderDescriptor.ProviderInstance ?? services.GetRequiredService(tokenProviderDescriptor.ProviderType)) as IUserTwoFactorTokenProvider<TUser>;
            //        if (userTwoFactorTokenProvider != null)
            //        {
            //            RegisterTokenProvider(key, userTwoFactorTokenProvider);
            //        }
            //    }
            //}

            //if (Options.Stores.ProtectPersonalData)
            //{
            //    if (!(Store is IProtectedUserStore<TUser>))
            //    {
            //        throw new InvalidOperationException(Resources.StoreNotIProtectedUserStore);
            //    }

            //    if (services.GetService<ILookupProtector>() == null)
            //    {
            //        throw new InvalidOperationException(Resources.NoPersonalDataProtector);
            //    }
            //}
        }
    }
}
