﻿using FortCode.Security;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Headers;
using System.Security.Claims;
using System.Text;
using System.Text.Encodings.Web;
using System.Threading.Tasks;

namespace FortCode
{

    public class BasicAuthenticationHandler : AuthenticationHandler<AuthenticationSchemeOptions>
    {
        
        private readonly SignInManager<FortCodeUserIdentity> _signInManager;
        private readonly IPasswordHasher<FortCodeUserIdentity> _passwordHasher;
        private readonly UserManager<FortCodeUserIdentity> _userManager;

        public BasicAuthenticationHandler(SignInManager<FortCodeUserIdentity> signInManager,
            IPasswordHasher<FortCodeUserIdentity> passwordHasher,
            UserManager<FortCodeUserIdentity> userManager,
            IOptionsMonitor<AuthenticationSchemeOptions> options,
            ILoggerFactory logger,
            UrlEncoder encoder,
            ISystemClock clock)
            : base(options, logger, encoder, clock)
        {
            _signInManager = signInManager ?? throw new ArgumentNullException(nameof(signInManager));
            _passwordHasher = passwordHasher ?? throw new ArgumentNullException(nameof(passwordHasher));
            _userManager = userManager ?? throw new ArgumentNullException(nameof(userManager));
        }
    
        protected override async Task<AuthenticateResult> HandleAuthenticateAsync()
        {
            try
            {
                var authHeader = AuthenticationHeaderValue.Parse(Request.Headers["Authorization"]);
                if (authHeader?.Parameter != null)
                {
                    var credentials = Encoding.UTF8.GetString(Convert.FromBase64String(authHeader.Parameter)).Split(':');
                    FortCodeUserIdentity userIdentity = new FortCodeUserIdentity();

                    string? username = credentials?.FirstOrDefault();

                    string? password = credentials?.LastOrDefault();
                    if (!string.IsNullOrWhiteSpace(username))
                    {

                        userIdentity.UserName = username;
                        userIdentity.PasswordHash = _passwordHasher.HashPassword(userIdentity, password);

                        SignInResult signResult = await _signInManager.CheckPasswordSignInAsync(userIdentity, userIdentity.PasswordHash, false);

                        if (signResult.Succeeded)
                        {

                            FortCodeUserIdentity fortCodeUser = await _userManager.FindByNameAsync(username);

                            ClaimsIdentity identity = new ClaimsIdentity();
                            identity.AddClaim(new Claim(ClaimTypes.NameIdentifier, fortCodeUser.UserName));
                            identity.AddClaim(new Claim(ClaimTypes.Name, fortCodeUser.UserName));
                            identity.AddClaim(new Claim(ClaimTypes.Email, fortCodeUser.Email));
                            identity.AddClaim(new Claim(ClaimTypes.Sid, fortCodeUser.Id.ToString()));
                            ClaimsPrincipal claimsPrin = new ClaimsPrincipal(identity);
                            return AuthenticateResult.Success(new AuthenticationTicket(claimsPrin, "Basic"));

                        }
                        return AuthenticateResult.Fail($"unrecognized credentials");
                    }

                    return AuthenticateResult.Fail($"username not found");
                }
                
                return AuthenticateResult.Fail($"Authorization header not found");
            }
            catch (Exception ex)
            {
                return AuthenticateResult.Fail($"Authentication failed: {ex.Message}");
            }
        }

    }
}
