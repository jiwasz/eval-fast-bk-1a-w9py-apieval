﻿using FortCode.ModelsV1;
using FortCode.RepositoryV1;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace FortCode.ControllerV1
{

    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [Authorize(AuthenticationSchemes = "Basic")]
    [Route("api/v1.0/countries")]
    [ApiController]
    public class CountryControllerV1 : ControllerBase
    {
        private ICountryRepositoryV1 _countryRep;

        public CountryControllerV1(ICountryRepositoryV1 countryRep)
        {
            _countryRep = countryRep ?? throw new ArgumentNullException(nameof(countryRep));
        }
        // GET: api/v1.0/<CountryController>
        //[HttpGet]
        //public IEnumerable<string> Get()
        //{
        //    return new string[] { "value1", "value2" };
        //}

        // GET api/v1.0/<CountryController>/5

        [HttpGet("{id}")]
        public async Task<ActionResult<GetCountryResponseV1>> GetAsync([FromRoute] Guid id)
        {
            GetCountryResponseV1 countryResult = await  _countryRep.GetCountryAsync(id);

            if(countryResult == null)
            {
                return new NotFoundResult();
            }

            return countryResult;
        }

    
    }
}
