﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Threading.Tasks;
//using Microsoft.AspNetCore.Http;
//using Microsoft.AspNetCore.Mvc;
//using Microsoft.EntityFrameworkCore;
//using FortCode.Data;

//namespace FortCode.ControllerV1
//{
//    [Route("api/[controller]")]
//    [ApiController]
//    public class DataCitiesController : ControllerBase
//    {
//        private readonly FortCodeDbContext _context;

//        public DataCitiesController(FortCodeDbContext context)
//        {
//            _context = context;
//        }

//        // GET: api/DataCities
//        [HttpGet]
//        public async Task<ActionResult<IEnumerable<DataCity>>> GetCities()
//        {
//            return await _context.Cities.ToListAsync();
//        }

//        // GET: api/DataCities/5
//        [HttpGet("{id}")]
//        public async Task<ActionResult<DataCity>> GetDataCity(Guid id)
//        {
//            var dataCity = await _context.Cities.FindAsync(id);

//            if (dataCity == null)
//            {
//                return NotFound();
//            }

//            return dataCity;
//        }

//        // PUT: api/DataCities/5
//        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
//        [HttpPut("{id}")]
//        public async Task<IActionResult> PutDataCity(Guid id, DataCity dataCity)
//        {
//            if (id != dataCity.Id)
//            {
//                return BadRequest();
//            }

//            _context.Entry(dataCity).State = EntityState.Modified;

//            try
//            {
//                await _context.SaveChangesAsync();
//            }
//            catch (DbUpdateConcurrencyException)
//            {
//                if (!DataCityExists(id))
//                {
//                    return NotFound();
//                }
//                else
//                {
//                    throw;
//                }
//            }

//            return NoContent();
//        }

//        // POST: api/DataCities
//        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
//        [HttpPost]
//        public async Task<ActionResult<DataCity>> PostDataCity(DataCity dataCity)
//        {
//            _context.Cities.Add(dataCity);
//            try
//            {
//                await _context.SaveChangesAsync();
//            }
//            catch (DbUpdateException)
//            {
//                if (DataCityExists(dataCity.Id))
//                {
//                    return Conflict();
//                }
//                else
//                {
//                    throw;
//                }
//            }

//            return CreatedAtAction("GetDataCity", new { id = dataCity.Id }, dataCity);
//        }

//        // DELETE: api/DataCities/5
//        [HttpDelete("{id}")]
//        public async Task<IActionResult> DeleteDataCity(Guid id)
//        {
//            var dataCity = await _context.Cities.FindAsync(id);
//            if (dataCity == null)
//            {
//                return NotFound();
//            }

//            _context.Cities.Remove(dataCity);
//            await _context.SaveChangesAsync();

//            return NoContent();
//        }

//        private bool DataCityExists(Guid id)
//        {
//            return _context.Cities.Any(e => e.Id == id);
//        }
//    }
//}
