﻿using FortCode.Security;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Threading.Tasks;

namespace FortCode.Controller
{


    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiController]
    [Route("api/v1.0/account")]
    public class AccountControllerV1 : ControllerBase
    {
        private readonly UserManager<FortCodeUserIdentity> _userManager;
        private readonly SignInManager<FortCodeUserIdentity> _signInManager;
        private readonly IPasswordHasher<FortCodeUserIdentity> _passwordHasher;
        private readonly ILogger<AccountControllerV1> _logger;

        public AccountControllerV1(
            UserManager<FortCodeUserIdentity> userManager,
            SignInManager<FortCodeUserIdentity> signInManager,
            IPasswordHasher<FortCodeUserIdentity> passwordHasher,
            ILogger<AccountControllerV1> logger)
        {
            _userManager = userManager ?? throw new ArgumentNullException(nameof(userManager));
            _signInManager = signInManager ?? throw new ArgumentNullException(nameof(signInManager));
            _passwordHasher = passwordHasher ?? throw new ArgumentNullException(nameof(passwordHasher));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        [AllowAnonymous]
        [HttpPost]       
        public async Task<ActionResult> CreateUserAsync([FromBody] UserCreateRequest userCreateRequest)
        {

            FortCodeUserIdentity newUser = new FortCodeUserIdentity();
            newUser.UserName = userCreateRequest.UserName;
            newUser.Id = Guid.NewGuid().ToString();
            newUser.PasswordHash = _passwordHasher.HashPassword(newUser, userCreateRequest.Password);

            try
            {
                IdentityResult result = await _userManager.CreateAsync(newUser);

                if (result.Succeeded)
                {
                    _logger.LogInformation($"Created user {userCreateRequest.UserName}");
                    return Ok();
                }
                else
                {
                    _logger.LogInformation($"Error creating user {userCreateRequest.UserName}", result.Errors);
                    return this.Problem(result.Errors.ToList()[0].Description);
                }    
            }
            catch(Exception ex)
            {
                _logger.LogError($"Unexpected error creating user {userCreateRequest.UserName}", ex);
                return this.Problem();
            }
        }


        //[AllowAnonymous]
        //[HttpPost]
        //public async Task<ActionResult> SignInAsync([FromBody] UserCreateRequest userCreateRequest)
        //{

        //    FortCodeUserIdentity newUser = new FortCodeUserIdentity();
        //    newUser.UserName = userCreateRequest.UserName;
        //    newUser.Id = Guid.NewGuid().ToString();
        //    newUser.PasswordHash = _passwordHasher.HashPassword(newUser, userCreateRequest.Password);

        //    try
        //    {
        //        IdentityResult result = await _userManager.CreateAsync(newUser);

        //        if (result.Succeeded)
        //        {
        //            _logger.LogInformation($"Created user {userCreateRequest.UserName}");
        //            return Ok();
        //        }
        //        else
        //        {
        //            _logger.LogInformation($"Error creating user {userCreateRequest.UserName}", result.Errors);
        //            return this.Problem(result.Errors.ToList()[0].Description);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        _logger.LogError($"Unexpected error creating user {userCreateRequest.UserName}", ex);
        //        return this.Problem();
        //    }
        //}


    }
}
