using FortCode.Data;
using FortCode.Security;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.Extensions.Configuration;
using FortCode.RepositoryV1;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

using Microsoft.OpenApi.Models;
using System;
using Microsoft.AspNetCore.Authentication;

namespace FortCode
{
    public class Startup
    {

        private const string ENV_DBCONNECTION = "DbContext";

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
            
        }

        public IConfiguration Configuration { get; }


        public virtual void ConfigureServices(IServiceCollection services)
        {




            // options.UseSqlite(
            services.AddDbContext<FortCodeDbContext>(options =>
                options.UseSqlServer(
                    Configuration.GetConnectionString(ENV_DBCONNECTION), x => x.MigrationsHistoryTable(FortCodeDbContext.ContextMigrationTable, FortCodeDbContext.ContextSchema))
                );

            services.AddDatabaseDeveloperPageExceptionFilter();


            services.Configure<PasswordHasherOptions>(options => options.CompatibilityMode = PasswordHasherCompatibilityMode.IdentityV3);
       // https://chsakell.com/2018/04/28/asp-net-core-identity-series-getting-started/

            services.AddIdentity<FortCodeUserIdentity, IdentityRole>()
                    .AddEntityFrameworkStores<FortCodeDbContext>();

            services.AddIdentityCore<FortCodeUserIdentity>(options => {
                // Password settings.
                options.Password.RequireDigit = true;
                options.Password.RequireLowercase = true;
                options.Password.RequireNonAlphanumeric = true;
                options.Password.RequireUppercase = true;
                options.Password.RequiredLength = 6;
                options.Password.RequiredUniqueChars = 3;

                
                // Lockout settings.
                options.Lockout.DefaultLockoutTimeSpan = TimeSpan.FromMinutes(5);
                options.Lockout.MaxFailedAccessAttempts = 5;
                options.Lockout.AllowedForNewUsers = true;

                // User settings.
                options.User.AllowedUserNameCharacters =
                "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-._@+";
                options.User.RequireUniqueEmail = true;

            });


            new IdentityBuilder(typeof(FortCodeUserIdentity), typeof(IdentityRole), services)
                .AddRoleManager<RoleManager<IdentityRole>>()
                .AddSignInManager<SignInManager<FortCodeUserIdentity>>()
                .AddEntityFrameworkStores<FortCodeDbContext>();


            //services.AddIdentityServer()
            //    .AddApiAuthorization<ApplicationUser, FortCodeDbContext>();

            services.AddTransient<ICountryRepositoryV1, CountryRepositoryV1>();
            services.AddTransient<ICityRepositoryV1, CityRepositoryV1>();


            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "FortCode", Version = "v1" });
                c.AddSecurityDefinition("basic", new OpenApiSecurityScheme
                {
                    Name = "Authorization",
                    Type = SecuritySchemeType.Http,
                    Scheme = "basic",
                    In = ParameterLocation.Header,
                    Description = "Basic Authorization header using the Bearer scheme."
                });
                c.AddSecurityRequirement(new OpenApiSecurityRequirement
                {
                    {
                          new OpenApiSecurityScheme
                            {
                                Reference = new OpenApiReference
                                {
                                    Type = ReferenceType.SecurityScheme,
                                    Id = "basic"
                                }
                            },
                            new string[] {}
                    }
                });            
            });

            services.AddAuthentication("Basic")
                    .AddScheme<AuthenticationSchemeOptions, BasicAuthenticationHandler>("Basic", null);

            services
                .AddMvc();

            services
                .AddControllers();
        }

        public virtual void Configure(IApplicationBuilder app, IWebHostEnvironment env, ILoggerFactory loggerFactory)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "FortCode v1"));
                app.UseRouteDebugger();
            }

            app
                .UseFileServer()
                .UseAuthentication()
                .UseRouting()
                .UseAuthorization()
                .UseEndpoints(endPoints => { endPoints.MapControllers(); });

           
            UpgradeDatabase(app);
        }

        private void UpgradeDatabase(IApplicationBuilder app)
        {
            using (var serviceScope = app.ApplicationServices.CreateScope())
            {
                var context = serviceScope.ServiceProvider.GetService<FortCodeDbContext>();
                if (context != null && context.Database != null)
                {
                    context.Database.Migrate();
                }
            }
        }
    }
}
