using FortCode.Data;
using FortCode.ModelsV1;
using FortCode.RepositoryV1;
using System;
using System.Threading.Tasks;
using Xunit;

namespace FortCode.Repository.Tests
{
    [Collection("Database collection")]
    public class CountryRepositoryTests
    {
        DatabaseFixture _fixture;



        public CountryRepositoryTests(DatabaseFixture fixture)
        {

            _fixture = fixture ?? throw new ArgumentNullException(nameof(fixture));
        }



        [Fact]
        public async Task CountryId_BadAsync()
        {
            Guid emptyGuid = new Guid();
            ICountryRepositoryV1 countryRep = GetCountryRepository();

            await Assert.ThrowsAsync<ArgumentException>(async () => 
                await countryRep.GetCountryAsync(emptyGuid).ConfigureAwait(false))
                    .ConfigureAwait(false);
        }


        [Fact]
        public async Task CountryId_NotExistsAsync()
        {

            Guid noCountryIdForOldDevelopers = Guid.NewGuid();

            ICountryRepositoryV1 countryRep = GetCountryRepository();

            GetCountryResponseV1 retCountry = await countryRep.GetCountryAsync(noCountryIdForOldDevelopers)
                .ConfigureAwait(false);

            Assert.NotNull(retCountry);

            Assert.Null(retCountry.Country);
        }


        [Fact]
        public async Task CountryId_ExistsAsync()
        {
            ICountryRepositoryV1 countryRep = GetCountryRepository();

            GetCountryResponseV1 retCountry = await countryRep.GetCountryAsync(TestConstants.COUNTRYID_ARUBA).ConfigureAwait(false);

            Assert.NotNull(retCountry?.Country);

            Assert.Equal("Aruba", retCountry.Country.Name);
        }

        [Fact]
        public async Task CountryName_ExistsAsync()
        {        
            ICountryRepositoryV1 countryRep = GetCountryRepository();

            GetCountryResponseV1 retCountry = await countryRep.GetCountryAsync("United States of America").ConfigureAwait(false);

            Assert.NotNull(retCountry.Country);

            Assert.Equal(TestConstants.COUNTRYID_USA, retCountry.Country.Id);
        }


        [Fact]
        public async Task CountryName_NotExistsAsync()
        {
            ICountryRepositoryV1 countryRep = GetCountryRepository();

            GetCountryResponseV1 retCountry = await countryRep.GetCountryAsync("Wakanda").ConfigureAwait(false);

            Assert.NotNull(retCountry);

            Assert.Null(retCountry.Country);
        }


        [Fact]
        public async Task CountryArg_ExceptionsAsync()
        {
            ICountryRepositoryV1 countryRep = GetCountryRepository();


            await Assert.ThrowsAsync<ArgumentException>(async () => 
                await countryRep.GetCountryAsync("   ").ConfigureAwait(false))
                    .ConfigureAwait(false);


            await Assert.ThrowsAsync<ArgumentNullException>(async () =>
                await countryRep.GetCountryAsync(null).ConfigureAwait(false))
                    .ConfigureAwait(false);

            await Assert.ThrowsAsync<ArgumentException>(async () =>
                await countryRep.GetCountryAsync(new Guid()).ConfigureAwait(false))
                    .ConfigureAwait(false);


            Assert.Throws<ArgumentNullException>(() => new CountryRepositoryV1(null));
        }


        private ICountryRepositoryV1 GetCountryRepository()
        {
            ICountryRepositoryV1 countryRep = new CountryRepositoryV1(_fixture.Db);

            return countryRep;
        }
    }
}
