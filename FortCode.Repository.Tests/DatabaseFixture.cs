﻿using System;
using System.Collections.Generic;
using System.Text;
using FortCode.Repository;
using FortCode.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace FortCode.Repository.Tests
{
    public class DatabaseFixture : IDisposable
    {
        private const string TESTDBSETTING = "testdb";

        private FortCodeDbContext _dbContext;

        public DatabaseFixture()
        {

            var config = new ConfigurationBuilder()
                .SetBasePath(AppContext.BaseDirectory)
                .AddJsonFile("appsettings.json", false, true)
                .Build();

            FortCodeDbContextFactory contextFactory = new FortCodeDbContextFactory();

            _dbContext = contextFactory.CreateDbContext(config.GetConnectionString(TESTDBSETTING));

            if(_dbContext.Database.CanConnect())
                _dbContext.Database.EnsureDeleted();

            _dbContext.Database.Migrate();

            Db = _dbContext;
        }

        public FortCodeDbContext Db { get; private set; }

        public void Dispose()
        {
            _dbContext.Database.EnsureDeleted();            
            _dbContext.Dispose();           
        }

    }
}
