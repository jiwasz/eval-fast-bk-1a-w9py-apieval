﻿using FortCode.RepositoryV1;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace FortCode.Repository.Tests
{

    [Collection("Database collection")]
    public class CityRepositoryTests
    {

        DatabaseFixture _fixture;

        public CityRepositoryTests(DatabaseFixture fixture)
        {

            _fixture = fixture ?? throw new ArgumentNullException(nameof(fixture));
        }


        [Fact]
        public async Task AddFavoriteCityAsync()
        {
            ICityRepositoryV1 cityRep = GetCityRepository();

            string city = "Philadelphia";

            string userId = Guid.NewGuid().ToString();

            Guid countryId = TestConstants.COUNTRYID_USA;


            //await cityRep.  
            //await cityRep.AddUserFavoriteCityAsync(city, countryId, userId);
         
        }

        private ICityRepositoryV1 GetCityRepository()
        {
            ICityRepositoryV1 cityRep = new CityRepositoryV1(_fixture.Db);

            return cityRep;
        }
    }
}
