﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FortCode.Repository.Tests
{
    internal class TestConstants
    {

        internal static readonly Guid COUNTRYID_USA = new Guid("36075c97-c164-4378-8412-7d62baab1017");

        internal static readonly Guid COUNTRYID_ARUBA = new Guid("faf4157f-7aa2-4920-8961-8e570a5cd91e");
    }
}
