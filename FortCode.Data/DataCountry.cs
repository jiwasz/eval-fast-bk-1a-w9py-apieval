﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics;
using System.Runtime.Serialization;
using System.Text;

namespace FortCode.Data
{

    [DataContract]
    [DebuggerDisplay("Id = {Id}, Name = {Name}")]
    [Table("countries")]
    public class DataCountry
    {

        [DataMember]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        [System.ComponentModel.DataAnnotations.Key]
        [Column("id", Order = 0)]
        public Guid Id { get; set; }


        [Required]
        [DataMember]
        [Column("name", Order = 1)]
        public string Name { get; set; }

        
        [Required]
        [DataMember]        
        [Column("code", Order = 2)]
        public string Code { get; set; }


        public IEnumerable<DataCity> Cities { get; set; } = new List<DataCity>();


    }
}
