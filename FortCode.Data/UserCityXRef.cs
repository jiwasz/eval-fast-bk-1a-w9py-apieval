﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics;
using System.Runtime.Serialization;
using System.Text;

namespace FortCode.Data
{

    [DataContract]
    [DebuggerDisplay("UserId = {UserId}, CityId = {CityId}")]
    [Table("usercityxref")]
    public class UserCityXRef
    {

        [Column("userid", Order = 0)]
        public string UserId { get; set; }

        [Column("cityid", Order = 1)]
        public Guid CityId { get; set; }

        public DataCity City { get; set; }
    }
}
