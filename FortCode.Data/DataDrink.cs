﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics;
using System.Runtime.Serialization;
using System.Text;

namespace FortCode.Data
{

    [DataContract]
    [DebuggerDisplay("Id = {Id}, Name = {Name}")]
    [Table("drinks")]
    public class DataDrink
    {
        [DataMember]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        [System.ComponentModel.DataAnnotations.Key]
        [Column("id", Order = 0)]
        public Guid Id { get; set; }

        [Required]
        [Column("barid", Order = 1)]
        public Guid BarId { get; set; }


        [Required]
        [DataMember]
        [Column("name", Order = 2)]
        public string Name { get; set; }

        public DataBar Bar { get; set; }

    }
}
