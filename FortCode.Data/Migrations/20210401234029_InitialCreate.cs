﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace FortCode.Data.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.EnsureSchema(
                name: "fortcode");

            migrationBuilder.CreateTable(
                name: "AspNetRoles",
                schema: "fortcode",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(256)", maxLength: 256, nullable: true),
                    NormalizedName = table.Column<string>(type: "nvarchar(256)", maxLength: 256, nullable: true),
                    ConcurrencyStamp = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetRoles", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUsers",
                schema: "fortcode",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    UserName = table.Column<string>(type: "nvarchar(256)", maxLength: 256, nullable: true),
                    NormalizedUserName = table.Column<string>(type: "nvarchar(256)", maxLength: 256, nullable: true),
                    Email = table.Column<string>(type: "nvarchar(256)", maxLength: 256, nullable: true),
                    NormalizedEmail = table.Column<string>(type: "nvarchar(256)", maxLength: 256, nullable: true),
                    EmailConfirmed = table.Column<bool>(type: "bit", nullable: false),
                    PasswordHash = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    SecurityStamp = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ConcurrencyStamp = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    PhoneNumber = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    PhoneNumberConfirmed = table.Column<bool>(type: "bit", nullable: false),
                    TwoFactorEnabled = table.Column<bool>(type: "bit", nullable: false),
                    LockoutEnd = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: true),
                    LockoutEnabled = table.Column<bool>(type: "bit", nullable: false),
                    AccessFailedCount = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUsers", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "bars",
                schema: "fortcode",
                columns: table => new
                {
                    id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    name = table.Column<string>(type: "nvarchar(max)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_bars", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "countries",
                schema: "fortcode",
                columns: table => new
                {
                    id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    name = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    code = table.Column<string>(type: "nvarchar(max)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_countries", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "datauditlog",
                schema: "fortcode",
                columns: table => new
                {
                    message = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    category = table.Column<int>(type: "int", nullable: false),
                    userid = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    auditdate = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                });

            migrationBuilder.CreateTable(
                name: "AspNetRoleClaims",
                schema: "fortcode",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    RoleId = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    ClaimType = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ClaimValue = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetRoleClaims", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AspNetRoleClaims_AspNetRoles_RoleId",
                        column: x => x.RoleId,
                        principalSchema: "fortcode",
                        principalTable: "AspNetRoles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserClaims",
                schema: "fortcode",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    UserId = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    ClaimType = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ClaimValue = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserClaims", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AspNetUserClaims_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalSchema: "fortcode",
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserLogins",
                schema: "fortcode",
                columns: table => new
                {
                    LoginProvider = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    ProviderKey = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    ProviderDisplayName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    UserId = table.Column<string>(type: "nvarchar(450)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserLogins", x => new { x.LoginProvider, x.ProviderKey });
                    table.ForeignKey(
                        name: "FK_AspNetUserLogins_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalSchema: "fortcode",
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserRoles",
                schema: "fortcode",
                columns: table => new
                {
                    UserId = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    RoleId = table.Column<string>(type: "nvarchar(450)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserRoles", x => new { x.UserId, x.RoleId });
                    table.ForeignKey(
                        name: "FK_AspNetUserRoles_AspNetRoles_RoleId",
                        column: x => x.RoleId,
                        principalSchema: "fortcode",
                        principalTable: "AspNetRoles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AspNetUserRoles_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalSchema: "fortcode",
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserTokens",
                schema: "fortcode",
                columns: table => new
                {
                    UserId = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    LoginProvider = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    Value = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserTokens", x => new { x.UserId, x.LoginProvider, x.Name });
                    table.ForeignKey(
                        name: "FK_AspNetUserTokens_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalSchema: "fortcode",
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "drinks",
                schema: "fortcode",
                columns: table => new
                {
                    id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    barid = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    name = table.Column<string>(type: "nvarchar(max)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_drinks", x => x.id);
                    table.ForeignKey(
                        name: "FK_drinks_bars_barid",
                        column: x => x.barid,
                        principalSchema: "fortcode",
                        principalTable: "bars",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "userbarxref",
                schema: "fortcode",
                columns: table => new
                {
                    userid = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    barid = table.Column<Guid>(type: "uniqueidentifier", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_user_bar", x => new { x.userid, x.barid });
                    table.ForeignKey(
                        name: "FK_userbarxref_bars_barid",
                        column: x => x.barid,
                        principalSchema: "fortcode",
                        principalTable: "bars",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "cities",
                schema: "fortcode",
                columns: table => new
                {
                    id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    countryid = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    name = table.Column<string>(type: "nvarchar(450)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_cities", x => x.id);
                    table.ForeignKey(
                        name: "FK_cities_countries_countryid",
                        column: x => x.countryid,
                        principalSchema: "fortcode",
                        principalTable: "countries",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "usercityxref",
                schema: "fortcode",
                columns: table => new
                {
                    userid = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    cityid = table.Column<Guid>(type: "uniqueidentifier", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_user_city", x => new { x.userid, x.cityid });
                    table.ForeignKey(
                        name: "FK_usercityxref_cities_cityid",
                        column: x => x.cityid,
                        principalSchema: "fortcode",
                        principalTable: "cities",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                schema: "fortcode",
                table: "countries",
                columns: new[] { "id", "code", "name" },
                values: new object[,]
                {
                    { new Guid("5a2f910f-f69a-4d3d-8816-3cd3f4e5285f"), "AF", "Afghanistan" },
                    { new Guid("fb0cc480-09ba-4391-99a7-8f28cd3c66aa"), "TH", "Thailand" },
                    { new Guid("1e1c541b-018b-405a-a7b2-dbb35678336f"), "TL", "Timor-Leste (East Timor)" },
                    { new Guid("fbd5dd47-1850-4c66-bd28-2ba8316db7e1"), "TG", "Togo" },
                    { new Guid("0e7a9a9e-c4ec-4a66-b91e-54a52db7ebb7"), "TO", "Tonga" },
                    { new Guid("d209506e-8fe9-4b41-9c20-268e74163a3d"), "TT", "Trinidad and Tobago" },
                    { new Guid("8cb5b12a-c5ca-4ec1-af15-76d710454b03"), "TN", "Tunisia" },
                    { new Guid("594bc4c6-ef41-4ff5-9ebc-cb077e62510a"), "TR", "Turkey" },
                    { new Guid("5980c8ef-6b8c-46ca-a512-039c8534e057"), "TM", "Turkmenistan" },
                    { new Guid("4c289b04-1516-4ab6-bcb8-96cd933c9a8b"), "TV", "Tuvalu" },
                    { new Guid("7750a47c-abd0-4d40-8371-6b86341efd02"), "UG", "Uganda" },
                    { new Guid("8637ce33-1c60-4eaa-bdf9-bf154f9cfbcf"), "UA", "Ukraine" },
                    { new Guid("859375ab-06c8-4e86-a649-bc6f6d9e3a8b"), "AE", "United Arab Emirates" },
                    { new Guid("b30459b1-50c5-4c30-836f-3f43954ee4b2"), "GB", "United Kingdom" },
                    { new Guid("91e191bc-69de-4773-8fa6-1e07edfa42a3"), "UY", "Uruguay" },
                    { new Guid("3350a890-a261-4a23-a741-a7cba7d0be46"), "UZ", "Uzbekistan" },
                    { new Guid("c696624f-bbf7-41d4-9f08-fb2191214796"), "VU", "Vanuatu" },
                    { new Guid("c09a205e-08d1-4a9c-bc5d-7767cf6588e5"), "VA", "Vatican City" },
                    { new Guid("8951fff4-4bee-43f1-b526-cb958fa938be"), "VE", "Venezuela" },
                    { new Guid("0dd0e7a5-cfbf-498e-96dc-5446d116631f"), "VN", "Vietnam" },
                    { new Guid("01e9795b-1d2a-416c-bd3b-97e01835eaae"), "YE", "Yemen" },
                    { new Guid("931569fd-2d44-4a57-8d2e-5b3e8ac58c1f"), "ZM", "Zambia" },
                    { new Guid("00fcaa09-478b-4e92-bba0-4150d54aa3fd"), "ZW", "Zimbabwe" },
                    { new Guid("5f35a545-9b34-4fbe-8c84-81bba2daee91"), "GE", "Abkhazia" },
                    { new Guid("0fc22eae-62ba-4670-a786-31c1bca138a1"), "TW", "Taiwan" },
                    { new Guid("11805434-cf92-4177-83b8-a6672173be4c"), "AZ", "Nagorno-Karabakh" },
                    { new Guid("6fd0110c-dc4f-4dd9-b388-4b60d046f832"), "CY", "Northern Cyprus" },
                    { new Guid("317c2962-790c-4b6d-b34b-d0c0eb4bdcd7"), "MD", "Pridnestrovie (Transnistria)" },
                    { new Guid("20be089c-3cdc-4571-87ee-d11e8c197eaf"), "SO", "Somaliland" },
                    { new Guid("f27b34c1-5d72-4c42-8201-0cea8a5889ca"), "GE", "South Ossetia" },
                    { new Guid("ff631bfb-c01f-433a-bf58-c7eb701ec338"), "TZ", "Tanzania" },
                    { new Guid("d79da204-87cb-4241-b0ff-77f804676a00"), "TJ", "Tajikistan" },
                    { new Guid("05d22bcb-9d7d-40dd-82cf-16950116e4dd"), "SY", "Syria" },
                    { new Guid("5f562dcf-bd00-42cd-8d71-48aa25d3f8e2"), "CH", "Switzerland" },
                    { new Guid("23b9b94e-33fd-4ff2-8158-83d1ecec8c62"), "PH", "Philippines" },
                    { new Guid("e7591ccd-b3ed-4343-b1a8-7b46f5b41b55"), "PL", "Poland" },
                    { new Guid("d174b97e-ef9e-4e1d-8f66-5c808da43f21"), "PT", "Portugal" },
                    { new Guid("525fb20d-0ade-4224-a371-cfd5de01ed32"), "QA", "Qatar" },
                    { new Guid("e5078f9c-63fb-49a9-b64b-5330c0cc8396"), "RO", "Romania" },
                    { new Guid("e256b332-be66-49e1-afcd-ad98f873b104"), "RU", "Russia" },
                    { new Guid("f298384e-10bf-45bf-82d2-fcf0cd9df6b5"), "RW", "Rwanda" },
                    { new Guid("d293a4d0-1f7a-4243-955b-62456891b9f5"), "KN", "Saint Kitts and Nevis" }
                });

            migrationBuilder.InsertData(
                schema: "fortcode",
                table: "countries",
                columns: new[] { "id", "code", "name" },
                values: new object[,]
                {
                    { new Guid("ad66f617-661e-43d2-83ce-021ceb3d95f9"), "LC", "Saint Lucia" },
                    { new Guid("976df2bb-e637-40bd-9498-6424760f5c54"), "VC", "Saint Vincent and the Grenadines" },
                    { new Guid("48d8ea4d-dcf1-4ea3-b1ea-2a1738c13a28"), "WS", "Samoa" },
                    { new Guid("f9f8b5d6-9647-4f81-9fb4-d92a10cb1cea"), "SM", "San Marino" },
                    { new Guid("3b6f1f7c-3e71-460e-9795-901da78147de"), "ST", "Sao Tome and Principe" },
                    { new Guid("17113480-9d86-494c-a147-5d119c5721a6"), "SA", "Saudi Arabia" },
                    { new Guid("9065815c-ae0d-4993-9aae-9a936dff0a72"), "AU", "Ashmore and Cartier Islands" },
                    { new Guid("3cd7704b-6365-4160-8b2c-6721af143c36"), "SN", "Senegal" },
                    { new Guid("d971042e-bb74-4669-b03b-434cb9925cb3"), "SC", "Seychelles" },
                    { new Guid("9edf4b65-5900-42dd-8826-33ad2edca3fa"), "SL", "Sierra Leone" },
                    { new Guid("47c827de-8e40-4b89-9f91-65ad7d73e3f6"), "SG", "Singapore" },
                    { new Guid("6ce14370-5bb1-4c28-a745-eccaf25a8fad"), "SK", "Slovakia" },
                    { new Guid("eb3fc456-b550-4ac6-bc80-6df971cbbc1d"), "SI", "Slovenia" },
                    { new Guid("38b903f4-c8e0-4d42-b22e-8d92112b364d"), "SB", "Solomon Islands" },
                    { new Guid("d3c47c1a-246f-48a4-b30a-0dd12d533414"), "SO", "Somalia" },
                    { new Guid("d0bf78fc-c3fa-418b-beeb-e2a00a40563c"), "ZA", "South Africa" },
                    { new Guid("1557074a-9dbf-4eeb-b78f-2a92261e66dd"), "ES", "Spain" },
                    { new Guid("71793d2a-1bc7-47b9-97df-296c25c4fb83"), "LK", "Sri Lanka" },
                    { new Guid("f422190b-3889-438a-bc15-702ad38499d4"), "SD", "Sudan" },
                    { new Guid("3ebbacac-53f7-4c5a-b145-e292090dc87c"), "SR", "Suriname" },
                    { new Guid("438ed187-e325-4757-b4da-dd73e36529a6"), "SZ", "Swaziland" },
                    { new Guid("3a4e11e7-b144-4cfe-b5b4-36e0cc37bab0"), "SE", "Sweden" },
                    { new Guid("09542b13-37ab-4972-bd0f-f7c767570a66"), "RS", "Serbia" },
                    { new Guid("49ac1eb0-3ac1-4d38-8580-6090dc31eb13"), "PE", "Peru" },
                    { new Guid("24ef3f6e-0a69-40b9-a4e6-83c71e54f115"), "CX", "Christmas Island" },
                    { new Guid("d68479b1-c20e-4d47-93be-f1ff409e06e7"), "AU", "Coral Sea Islands" },
                    { new Guid("77305f99-fd40-4bf0-9a36-bf7950dbf7bd"), "UM", "Baker Island" },
                    { new Guid("7ab94f09-ce52-47c2-82f0-8b78bb862915"), "GU", "Guam" },
                    { new Guid("5f7f9c55-4a3e-43e7-8ca7-6388ffa807b6"), "UM", "Howland Island" },
                    { new Guid("4f587a86-8a7f-4da9-b3f0-24b622817e44"), "UM", "Jarvis Island" },
                    { new Guid("543ddd76-cc7c-4cae-83cf-a0c0d07c6f86"), "UM", "Johnston Atoll" },
                    { new Guid("5b586b80-526e-4728-8df6-c847d5058132"), "UM", "Kingman Reef" },
                    { new Guid("f6af6844-7215-4567-908c-a313f283f8ab"), "UM", "Midway Islands" },
                    { new Guid("d01745f9-fab6-4e58-98f7-e63d3dc4b0b7"), "UM", "Navassa Island" },
                    { new Guid("95cb1508-b0ab-4902-9ca8-5ebaad10a6cc"), "UM", "Palmyra Atoll" },
                    { new Guid("086e2601-63e2-41bf-8297-c422168e2613"), "VI", "U.S. Virgin Islands" },
                    { new Guid("36075c97-c164-4378-8412-7d62baab1017"), "US", "United States of America" },
                    { new Guid("7e34791e-038a-4326-ad13-6d5a2297bca1"), "UM", "Wake Island" },
                    { new Guid("42323a3c-3dad-4cd9-a923-89db8acfe55f"), "HK", "Hong Kong" },
                    { new Guid("6a8bb59b-ab76-45a8-8759-66e15e5655ff"), "MO", "Macau" },
                    { new Guid("034b4167-44e2-4112-8ba3-31aa15f5205b"), "FO", "Faroe Islands" },
                    { new Guid("23dfa252-889e-4a0d-a6e2-cc019551f7cc"), "GL", "Greenland" }
                });

            migrationBuilder.InsertData(
                schema: "fortcode",
                table: "countries",
                columns: new[] { "id", "code", "name" },
                values: new object[,]
                {
                    { new Guid("03e43427-a235-4de2-90c4-6a8bf21c37fc"), "GF", "French Guiana" },
                    { new Guid("b0ce106a-eff0-4687-9c1f-15c10343465b"), "GP", "Guadeloupe" },
                    { new Guid("4c450ed7-e427-44c4-a650-bf73eb3acef7"), "MQ", "Martinique" },
                    { new Guid("df27e7f2-0576-4796-a1b8-f21192768c26"), "RE", "Reunion" },
                    { new Guid("c39a4ae8-38e4-473b-b0f0-6e6f07b841b7"), "AX", "Aland" },
                    { new Guid("faf4157f-7aa2-4920-8961-8e570a5cd91e"), "AW", "Aruba" },
                    { new Guid("df163f9b-4bac-43bd-b88b-87980488f3bb"), "AN", "Netherlands Antilles" },
                    { new Guid("54370255-7725-4d60-b8ae-038d9fef9431"), "SJ", "Svalbard" },
                    { new Guid("8602359e-ad06-491e-a0c0-f6d5f9acb39a"), "AC", "Ascension" },
                    { new Guid("8a906a3c-501a-465c-903e-5148f1407373"), "TA", "Tristan da Cunha" },
                    { new Guid("85ba3dd7-ca67-438c-acf5-49f7bd32f2aa"), "AQ", "Australian Antarctic Territory" },
                    { new Guid("ff97ef1b-fba8-4cd8-a94d-90aebfadc1b3"), "AQ", "Ross Dependency" },
                    { new Guid("5b244c75-7468-4264-b92c-4b3dfc9e2537"), "AQ", "Peter I Island" },
                    { new Guid("9dcc593f-0c77-4ca7-92c2-be99cc23029f"), "AS", "American Samoa" },
                    { new Guid("e704d8df-f22d-4d8f-aab3-2162ec874fa3"), "PR", "Puerto Rico" },
                    { new Guid("51505651-4287-4e18-9e7c-51a08aaf820e"), "MP", "Northern Mariana Islands" },
                    { new Guid("33e06ab7-e912-4e55-b4a6-9123121ab24a"), "TC", "Turks and Caicos Islands" },
                    { new Guid("2e11c26d-64be-472a-b361-80b88da75c00"), "HM", "Heard Island and McDonald Islands" },
                    { new Guid("fc8c4a18-8e03-4704-8995-fef0e89a3b37"), "NF", "Norfolk Island" },
                    { new Guid("e965f02c-1ce8-4873-b284-4f2efd3b4a11"), "NC", "New Caledonia" },
                    { new Guid("322ca87f-8f80-4b88-b746-4c89d05ac238"), "PF", "French Polynesia" },
                    { new Guid("84745fb8-0bd7-4974-add8-f9cd1c9361cc"), "YT", "Mayotte" },
                    { new Guid("e61db1d3-1a40-49d2-8a84-e2e3d2e42fab"), "GP", "Saint Barthelemy" },
                    { new Guid("4bb95775-fa76-4e23-a09b-1985fdcb36ba"), "GP", "Saint Martin" },
                    { new Guid("653a9d84-786d-4c47-a7b6-3fa7171f43bc"), "PM", "Saint Pierre and Miquelon" },
                    { new Guid("6672199e-052d-4d5b-8f36-6e741abae433"), "WF", "Wallis and Futuna" },
                    { new Guid("6f491b5f-4092-4485-b1f9-a40d8f62aff9"), "TF", "French Southern and Antarctic Lands" },
                    { new Guid("0a546c37-e168-4306-9193-78066cec2830"), "PF", "Clipperton Island" },
                    { new Guid("5a639cce-75dd-4238-9b50-25e4dd5aec0b"), "BV", "Bouvet Island" },
                    { new Guid("886ba1f8-548f-40ec-b11c-f5479ac561df"), "CK", "Cook Islands" },
                    { new Guid("7b466b7b-0c77-417d-acbf-8c6dbd72a5a5"), "NU", "Niue" },
                    { new Guid("0640fb0b-13d1-4629-abb8-bd5380e71444"), "CC", "Cocos (Keeling) Islands" },
                    { new Guid("915e39f9-364a-4720-90c4-b66198e523f2"), "TK", "Tokelau" },
                    { new Guid("8859d3b3-c950-480a-aa75-9fe4381de4d2"), "IM", "Isle of Man" },
                    { new Guid("4e7c406b-9e3c-40c2-be98-a948d814e7c4"), "JE", "Jersey" },
                    { new Guid("c4f1f8e6-0e3f-439d-8ade-d2076b11e341"), "AI", "Anguilla" },
                    { new Guid("c1d74346-e40b-4f47-8ef6-7553001dc557"), "BM", "Bermuda" },
                    { new Guid("995273b2-2ca2-4a11-9d22-881cbb1938e8"), "IO", "British Indian Ocean Territory" },
                    { new Guid("3d01598a-5932-4039-bb1c-f6f8227cb73a"), "", "British Sovereign Base Areas" },
                    { new Guid("531d6e14-8a6a-47af-aba8-5a95e1631687"), "VG", "British Virgin Islands" },
                    { new Guid("67a08ac2-f2f5-4b14-bfdb-35dd3421b7ff"), "KY", "Cayman Islands" },
                    { new Guid("43cdedc9-d10e-4e1c-825a-c7453e265d9c"), "FK", "Falkland Islands (Islas Malvinas)" }
                });

            migrationBuilder.InsertData(
                schema: "fortcode",
                table: "countries",
                columns: new[] { "id", "code", "name" },
                values: new object[,]
                {
                    { new Guid("e78261ae-011b-40e6-9158-2b566d243c7a"), "GI", "Gibraltar" },
                    { new Guid("915f63d0-5a0c-4f54-b610-e060a1310bd9"), "MS", "Montserrat" },
                    { new Guid("6d67ab9a-a687-442a-a85e-7cb7cab1e577"), "PN", "Pitcairn Islands" },
                    { new Guid("83b24994-b820-47a2-999d-028b4192bdf1"), "SH", "Saint Helena" },
                    { new Guid("6843d514-94cd-4280-92c1-e1bd48ffb6ae"), "GS", "South Georgia & South Sandwich Islands" },
                    { new Guid("8e6fe28a-c785-4158-9ab9-8c902b5526e4"), "GG", "Guernsey" },
                    { new Guid("2d5dbc9a-5d4a-4629-82c8-806866df1454"), "PY", "Paraguay" },
                    { new Guid("5f836593-71b0-4bd2-ba01-b6723ce8ed20"), "PG", "Papua New Guinea" },
                    { new Guid("821b75d3-a96a-450f-a907-e6ad4c5ed6e9"), "PA", "Panama" },
                    { new Guid("91b8f6a0-f358-4127-9bc3-41a08d4ae1c1"), "CN", "People's Republic of China" },
                    { new Guid("7d4fda66-fbd6-480e-9bb9-59d763e56576"), "CO", "Colombia" },
                    { new Guid("a2ee490a-ae1f-4a42-9281-318309ebbb36"), "KM", "Comoros" },
                    { new Guid("fc28b0d3-b82e-48eb-8c8c-27f5971c0281"), "CD", "Congo - Kinshasa" },
                    { new Guid("147d4042-ee21-4b03-8ecd-44ddfa2bac64"), "CG", "Congo - Brazzaville" },
                    { new Guid("08e10e32-fc7e-4b1c-a261-bcfb9f65c464"), "CR", "Costa Rica" },
                    { new Guid("6c007f13-5f8e-4166-bd0e-7403e2691240"), "CI", "Cote d'Ivoire (The Ivory Coast)" },
                    { new Guid("09f1fc20-3ecc-4087-88b4-3cd52caef470"), "HR", "Croatia" },
                    { new Guid("f1d6d009-e884-4746-9286-7b07e6db810f"), "CU", "Cuba" },
                    { new Guid("f67e0dcb-11a3-4bb9-b9ea-3a8dc4b15f10"), "CY", "Cyprus" },
                    { new Guid("d4ed44e0-e56d-4432-b021-b23246616345"), "CZ", "Czech Republic" },
                    { new Guid("39467938-fbeb-4bff-a38c-73e86ccf3648"), "DK", "Denmark" },
                    { new Guid("e88f50e7-7c62-4fa0-8060-28e4a2feba30"), "DJ", "Djibouti" },
                    { new Guid("5a9d6a4e-3c34-4389-ad35-a39c7576d5ec"), "DM", "Dominica" },
                    { new Guid("557fd10e-ff16-4f7a-ac47-4f868029d0eb"), "DO", "Dominican Republic" },
                    { new Guid("fcaa3d62-fede-4e77-9054-13193dc50459"), "EC", "Ecuador" },
                    { new Guid("6974ea39-295a-4082-9050-711e9a10faa9"), "EG", "Egypt" },
                    { new Guid("dbe59b33-4018-46ca-8dde-23bc83d9c884"), "SV", "El Salvador" },
                    { new Guid("fed08559-d23d-44f3-8f67-012a04be598f"), "GQ", "Equatorial Guinea" },
                    { new Guid("19797ae3-2380-4192-8e7e-7d0d521aeecc"), "ER", "Eritrea" },
                    { new Guid("f4e31e1f-d8c1-4997-94fa-706858712733"), "EE", "Estonia" },
                    { new Guid("68aeaa8e-2dfa-486f-80a5-edc9c74fab19"), "ET", "Ethiopia" },
                    { new Guid("afea2b51-4bdc-4fcb-9d3e-2b5bdd837a39"), "FJ", "Fiji" },
                    { new Guid("443edd58-7e81-4816-b4da-95246d532757"), "FI", "Finland" },
                    { new Guid("de78da1b-7396-4cc4-9f8e-9af33a839850"), "FR", "France" },
                    { new Guid("056f92b8-e0e3-4224-898b-0f4208b56185"), "GA", "Gabon" },
                    { new Guid("ccdfe3bb-3c3a-4780-b222-f59c5b6c695c"), "GM", "Gambia" },
                    { new Guid("d87725fc-9065-4db5-a327-e8514976ec70"), "GE", "Georgia" },
                    { new Guid("25a19f12-05e8-4fbf-97ca-16d54a00c133"), "DE", "Germany" },
                    { new Guid("312ae8d1-cd29-4a43-b9a1-a2bd66055d0b"), "CL", "Chile" },
                    { new Guid("822e7e3e-6625-4d42-8cdc-44014f4158a7"), "TD", "Chad" },
                    { new Guid("ac0fef98-ad32-4e51-bcfa-86a6f0922a19"), "CF", "Central African Republic" },
                    { new Guid("0bf3e0b2-a310-42a4-9dc0-561350f64a75"), "CV", "Cape Verde" }
                });

            migrationBuilder.InsertData(
                schema: "fortcode",
                table: "countries",
                columns: new[] { "id", "code", "name" },
                values: new object[,]
                {
                    { new Guid("a1983046-9547-4bb8-ba1d-51634f25aa64"), "AL", "Albania" },
                    { new Guid("7db990b2-8e87-44ad-a337-aa560bdf0016"), "DZ", "Algeria" },
                    { new Guid("53345784-9c8e-42fa-b5fd-c804095fd66c"), "AD", "Andorra" },
                    { new Guid("540e9681-3413-40ab-8cfb-8aaa0851cd3b"), "AO", "Angola" },
                    { new Guid("24f24a10-96da-4390-86e4-7bc728e18b28"), "AG", "Antigua and Barbuda" },
                    { new Guid("b212c61f-c0bc-4b75-80cc-d2bb13eb12b5"), "AR", "Argentina" },
                    { new Guid("bb89b607-005e-4f29-96b1-8a35d8528ed3"), "AM", "Armenia" },
                    { new Guid("a4be67f7-767d-41d4-94bd-1b79bafacbf1"), "AU", "Australia" },
                    { new Guid("e7da4b25-57bd-4a32-aaa6-a2845182d32d"), "AT", "Austria" },
                    { new Guid("04a76e8f-9349-41db-b35a-b1b891a43e64"), "AZ", "Azerbaijan" },
                    { new Guid("50834a27-9d55-4742-b6ef-1d31bd1da401"), "BS", "Bahamas" },
                    { new Guid("de8c98fa-ec22-4681-add0-406b36e7dbb6"), "BH", "Bahrain" },
                    { new Guid("4a14193b-e51a-40a4-9d31-ec4059c3419a"), "BD", "Bangladesh" },
                    { new Guid("8b00cb8c-7ffa-4a0c-adb0-6694785323d1"), "BB", "Barbados" },
                    { new Guid("d096bb93-28cb-4548-a236-54080f9aa2df"), "GH", "Ghana" },
                    { new Guid("1ad877dc-d8bb-4db9-91ea-ad91664e02b3"), "BY", "Belarus" },
                    { new Guid("b322d4b0-e8eb-45f8-b8eb-4aff53e04516"), "BZ", "Belize" },
                    { new Guid("4bff0656-0133-42ea-b5a3-6ce109441116"), "BJ", "Benin" },
                    { new Guid("7dd78c3d-3c0a-4058-9ce2-70b887357afe"), "BT", "Bhutan" },
                    { new Guid("d23f0548-b592-41bc-b6d7-e6e23226e3b5"), "BO", "Bolivia" },
                    { new Guid("c2cdf4fd-1383-420f-8e70-c8c5f91308f5"), "BA", "Bosnia and Herzegovina" },
                    { new Guid("1e5d73ed-d9f5-401f-a0f3-155318fda56f"), "BW", "Botswana" },
                    { new Guid("a0660e22-ddb2-4520-9800-d001957d6315"), "BR", "Brazil" },
                    { new Guid("9995fe37-2be6-4c44-95e1-50651fb3171e"), "BN", "Brunei" },
                    { new Guid("0990e29a-20a3-4b5f-9973-1859b5434f14"), "BG", "Bulgaria" },
                    { new Guid("7bb43913-479f-48a7-956f-a80a2166dc3b"), "BF", "Burkina Faso" },
                    { new Guid("e4fdb20d-ece9-4e8b-affc-1be482b70810"), "BI", "Burundi" },
                    { new Guid("16c5d6a4-202a-4c50-8526-9af1f891198b"), "KH", "Cambodia" },
                    { new Guid("ac6f7d53-a044-467b-b3d6-170865d0375e"), "CM", "Cameroon" },
                    { new Guid("d78a6be0-bf99-44a1-9987-b9f54c55af9e"), "CA", "Canada" },
                    { new Guid("36ea0c91-f462-4ede-86d4-759850d42dda"), "BE", "Belgium" },
                    { new Guid("146af0f6-94de-458b-8f9f-87320602f1e1"), "GR", "Greece" },
                    { new Guid("0c8d70be-6ebc-43f7-9580-6e18305f71ef"), "GD", "Grenada" },
                    { new Guid("4ed9c5ad-268d-435a-a873-f4d7d811c7f9"), "GT", "Guatemala" },
                    { new Guid("3d60db86-a446-4ec7-bdee-74e78e9b014d"), "MG", "Madagascar" },
                    { new Guid("7a9b3bb4-cedd-4d57-998e-601eb28dec2c"), "MW", "Malawi" },
                    { new Guid("2c49d482-c5a4-43c8-a7c1-8acfbb6c7744"), "MY", "Malaysia" },
                    { new Guid("c234d65e-6013-4e62-bc50-bfe7cde2013f"), "MV", "Maldives" },
                    { new Guid("568cf02f-d700-448c-ab3a-f003a9ea00c4"), "ML", "Mali" },
                    { new Guid("2f0c4d6d-4276-4428-b697-74c391b2ee9e"), "MT", "Malta" },
                    { new Guid("281a2c09-644c-4f68-b0a6-d133c485e89b"), "MH", "Marshall Islands" },
                    { new Guid("66a8a2ed-aa93-40e4-8f3b-f468a85b090a"), "MR", "Mauritania" }
                });

            migrationBuilder.InsertData(
                schema: "fortcode",
                table: "countries",
                columns: new[] { "id", "code", "name" },
                values: new object[,]
                {
                    { new Guid("c31f7e5c-5d7d-47b6-a222-c9c667ff2a5b"), "MU", "Mauritius" },
                    { new Guid("c6c57770-2eff-4b79-8542-6902f1aa3699"), "MX", "Mexico" },
                    { new Guid("3fd59b61-bd4d-4f91-abe3-4e1a130d6e32"), "FM", "Micronesia" },
                    { new Guid("9ed009e4-26d8-43c6-a903-4d06d0519f15"), "MD", "Moldova" },
                    { new Guid("0e1a40e0-46ff-4151-a4d7-d8a285b5f9e6"), "MC", "Monaco" },
                    { new Guid("ebeb5778-0afd-42f4-9aff-f68acae6efc3"), "MN", "Mongolia" },
                    { new Guid("07a13ede-909d-499b-becd-a57719403002"), "MK", "Macedonia" },
                    { new Guid("e119e8e0-9917-41f7-a839-bb8463722dea"), "ME", "Montenegro" },
                    { new Guid("ad2aef56-2662-42da-bd71-c4400bf006f3"), "MZ", "Mozambique" },
                    { new Guid("7af40651-a239-4c18-b804-217fcd6e1eeb"), "MM", "Myanmar (Burma)" },
                    { new Guid("88a3d405-d451-4875-9e1c-f1e10e7504f3"), "NA", "Namibia" },
                    { new Guid("fe763d52-3177-4d64-8bc2-a3dc6f839f92"), "NR", "Nauru" },
                    { new Guid("55c0d5dc-297e-44a5-a660-ad80e44723b2"), "NP", "Nepal" },
                    { new Guid("ad62e8db-9be4-4a3e-813d-df5ef8dd6e0f"), "NL", "Netherlands" },
                    { new Guid("40707948-154d-4caf-8822-edb2323733b7"), "NZ", "New Zealand" },
                    { new Guid("004d49bf-186c-4ec1-b347-03a28e30836f"), "NI", "Nicaragua" },
                    { new Guid("85085e27-1a2f-40eb-b374-9ae94759f589"), "NE", "Niger" },
                    { new Guid("ade4a75b-2947-4f38-9256-fc5f10d5af1f"), "NG", "Nigeria" },
                    { new Guid("bc2585d5-49a8-431c-8725-36ea836352b4"), "NO", "Norway" },
                    { new Guid("6fe4e2cc-2f84-4479-960e-2f22fd432379"), "OM", "Oman" },
                    { new Guid("0176e9e4-44de-4abf-94aa-721b42ffcb04"), "PK", "Pakistan" },
                    { new Guid("88b13d59-cd09-491a-8a05-13eb46e68cbd"), "PW", "Palau" },
                    { new Guid("f5b6024b-bfca-4a7e-9f9e-3d6e267db940"), "MA", "Morocco" },
                    { new Guid("600702af-a65d-4a16-9c79-625446d4a0c4"), "AQ", "Queen Maud Land" },
                    { new Guid("bde6f0f8-2cb9-4382-9ce7-d1a16d82e6ab"), "LU", "Luxembourg" },
                    { new Guid("04aaa761-e4be-407b-9e58-038dc37e5c75"), "LI", "Liechtenstein" },
                    { new Guid("863c5203-5a15-4e76-ae94-5374c0a540b3"), "GN", "Guinea" },
                    { new Guid("5b24f2f0-a005-4ff6-967f-46835aaa1980"), "GW", "Guinea-Bissau" },
                    { new Guid("70310f65-3195-4d15-930b-4e6b1481c55b"), "GY", "Guyana" },
                    { new Guid("c9193655-2c28-4c4a-886e-349d0e01a36d"), "HT", "Haiti" },
                    { new Guid("bbdc411a-3808-42a1-90a8-7d089779694f"), "HN", "Honduras" },
                    { new Guid("e3104833-c663-49f1-86db-717721a26548"), "HU", "Hungary" },
                    { new Guid("983bc06a-82b8-47b8-8168-dc7d4d835858"), "IS", "Iceland" },
                    { new Guid("8083afc9-3f61-47a5-a829-b70b13131e03"), "IN", "India" },
                    { new Guid("1304519d-d148-4646-bc8c-e28161679fce"), "ID", "Indonesia" },
                    { new Guid("4a7c51fc-5abc-48b0-ad3b-f03390844a59"), "IR", "Iran" },
                    { new Guid("1f532da7-9de6-4bc0-8986-631823dce2c3"), "IQ", "Iraq" },
                    { new Guid("8539a888-2f1f-446b-b5d3-0fec4c45882f"), "IE", "Ireland" },
                    { new Guid("dbfeb2d8-28c5-4996-b0e9-b8a879a612a3"), "IL", "Israel" },
                    { new Guid("6f53b6d5-45c0-418c-bfd0-f1fbb1ed7fb7"), "IT", "Italy" },
                    { new Guid("1a0914ee-0300-4989-9750-64812b277abd"), "LT", "Lithuania" },
                    { new Guid("a0ab3462-dd53-486e-a083-46fe76095608"), "JM", "Jamaica" }
                });

            migrationBuilder.InsertData(
                schema: "fortcode",
                table: "countries",
                columns: new[] { "id", "code", "name" },
                values: new object[,]
                {
                    { new Guid("b3b82e4d-d302-4ed5-88de-12e4988e1149"), "JO", "Jordan" },
                    { new Guid("055bbff3-1e42-42eb-af89-f1ec3b93db23"), "KZ", "Kazakhstan" },
                    { new Guid("ff2ad17b-4c9e-415a-840c-3fb9865be496"), "KE", "Kenya" },
                    { new Guid("72f0b3e3-480c-492b-8580-c12c8c4ec2af"), "KI", "Kiribati" },
                    { new Guid("066aeb4c-3c29-4cfd-a066-186f6538e848"), "KP", "North Korea" },
                    { new Guid("eda1c242-3508-4fa8-bdee-485d21a9267d"), "KR", "South Korea" },
                    { new Guid("9a884ee3-6293-484c-bacb-d4df12edc119"), "KW", "Kuwait" },
                    { new Guid("0a0c3abb-da66-48bd-b237-ce253e0c53bd"), "KG", "Kyrgyzstan" },
                    { new Guid("df6edd90-8387-42fe-ab45-5ef7f2f575ae"), "LA", "Laos" },
                    { new Guid("995d30b5-3bd4-48aa-8269-209f9ebc553f"), "LV", "Latvia" },
                    { new Guid("c9cd07e9-2056-4efb-aef3-533254b1818a"), "LB", "Lebanon" },
                    { new Guid("8f12f334-e2da-43ee-a143-8f2f371c514a"), "LS", "Lesotho" },
                    { new Guid("500e1480-f9e5-44bf-8133-7268a14b8941"), "LR", "Liberia" },
                    { new Guid("75ea52ed-875a-4c04-9e5f-52773e559796"), "LY", "Libya" },
                    { new Guid("61842bd1-7389-4845-a5e5-a2447ae7872b"), "JP", "Japan" },
                    { new Guid("236e0697-4c08-4347-a513-4f0049aef2cc"), "AQ", "British Antarctic Territory" }
                });

            migrationBuilder.CreateIndex(
                name: "IX_AspNetRoleClaims_RoleId",
                schema: "fortcode",
                table: "AspNetRoleClaims",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "RoleNameIndex",
                schema: "fortcode",
                table: "AspNetRoles",
                column: "NormalizedName",
                unique: true,
                filter: "[NormalizedName] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserClaims_UserId",
                schema: "fortcode",
                table: "AspNetUserClaims",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserLogins_UserId",
                schema: "fortcode",
                table: "AspNetUserLogins",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserRoles_RoleId",
                schema: "fortcode",
                table: "AspNetUserRoles",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "EmailIndex",
                schema: "fortcode",
                table: "AspNetUsers",
                column: "NormalizedEmail");

            migrationBuilder.CreateIndex(
                name: "UserNameIndex",
                schema: "fortcode",
                table: "AspNetUsers",
                column: "NormalizedUserName",
                unique: true,
                filter: "[NormalizedUserName] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "idx_city_country",
                schema: "fortcode",
                table: "cities",
                columns: new[] { "countryid", "name" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_drinks_barid",
                schema: "fortcode",
                table: "drinks",
                column: "barid");

            migrationBuilder.CreateIndex(
                name: "IX_userbarxref_barid",
                schema: "fortcode",
                table: "userbarxref",
                column: "barid");

            migrationBuilder.CreateIndex(
                name: "IX_usercityxref_cityid",
                schema: "fortcode",
                table: "usercityxref",
                column: "cityid");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AspNetRoleClaims",
                schema: "fortcode");

            migrationBuilder.DropTable(
                name: "AspNetUserClaims",
                schema: "fortcode");

            migrationBuilder.DropTable(
                name: "AspNetUserLogins",
                schema: "fortcode");

            migrationBuilder.DropTable(
                name: "AspNetUserRoles",
                schema: "fortcode");

            migrationBuilder.DropTable(
                name: "AspNetUserTokens",
                schema: "fortcode");

            migrationBuilder.DropTable(
                name: "datauditlog",
                schema: "fortcode");

            migrationBuilder.DropTable(
                name: "drinks",
                schema: "fortcode");

            migrationBuilder.DropTable(
                name: "userbarxref",
                schema: "fortcode");

            migrationBuilder.DropTable(
                name: "usercityxref",
                schema: "fortcode");

            migrationBuilder.DropTable(
                name: "AspNetRoles",
                schema: "fortcode");

            migrationBuilder.DropTable(
                name: "AspNetUsers",
                schema: "fortcode");

            migrationBuilder.DropTable(
                name: "bars",
                schema: "fortcode");

            migrationBuilder.DropTable(
                name: "cities",
                schema: "fortcode");

            migrationBuilder.DropTable(
                name: "countries",
                schema: "fortcode");
        }
    }
}
