﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FortCode.Security;
using Microsoft.Extensions.Logging;
using Microsoft.EntityFrameworkCore.Infrastructure;

namespace FortCode.Data
{
    public class FortCodeDbContext : IdentityDbContext<FortCodeUserIdentity>
    {
        public static readonly string ContextSchema = "fortcode";

        public static readonly string ContextMigrationTable = "__FortCodeMigrations";

        public DbSet<DataCity> Cities { get; set; }

        public DbSet<DataCountry> Countries { get; set; }


        public DbSet<DataDrink> Drinks { get; set; }

        public DbSet<DataBar> Bars { get; set; }

        public DbSet<UserBarXRef> UserBars { get; set; }

        public DbSet<UserCityXRef> UserCities { get; set; }

        public DbSet<DataAuditLog> AuditLog { get; set; }

        private readonly ILogger<FortCodeDbContext> _logger;

        public FortCodeDbContext(DbContextOptions<FortCodeDbContext> options, ILogger<FortCodeDbContext> logger)
            : base(options)
        {

            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {

#if DEBUG
            optionsBuilder.EnableSensitiveDataLogging();
            optionsBuilder.LogTo(Console.WriteLine);           
#endif

            base.OnConfiguring(optionsBuilder);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasDefaultSchema(ContextSchema);
            base.OnModelCreating(modelBuilder);


            modelBuilder.Entity<DataCity>()
            .HasOne<DataCountry>(x => x.Country)
            .WithMany(c => c.Cities)
            .HasForeignKey(u => u.CountryId);

            modelBuilder.Entity<DataCity>().HasIndex(
                c => new { c.CountryId, c.Name })
                    .IsUnique(true).HasDatabaseName("idx_city_country");

            IEnumerable<DataCountry> seedCountries = DataLoadUtlity.GetSeedDataContent<DataCountry>("countries");

            modelBuilder.Entity<DataCountry>().HasData(seedCountries);

            modelBuilder.Entity<UserBarXRef>()
            .HasOne<DataBar>(x => x.Bar)
            .WithMany(c => c.UserBars)
            .HasForeignKey(u => u.BarId);

            modelBuilder.Entity<UserCityXRef>()
            .HasOne<DataCity>(x => x.City)
            .WithMany(c => c.UserCities)
            .HasForeignKey(u => u.CityId);

            modelBuilder.Entity<DataDrink>()
            .HasOne<DataBar>(x => x.Bar)
            .WithMany(c => c.Drinks)
            .HasForeignKey(u => u.BarId);


            modelBuilder.Entity<UserBarXRef>()
              .HasKey(idx =>
                  new { idx.UserId, idx.BarId }).HasName("pk_user_bar");

            modelBuilder.Entity<UserCityXRef>()
                .HasKey(idx =>
                    new { idx.UserId, idx.CityId }).HasName("pk_user_city");

            modelBuilder.Entity<DataAuditLog>().HasNoKey();

        }
    }
}

