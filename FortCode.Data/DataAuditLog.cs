﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics;
using System.Runtime.Serialization;
using System.Text;

namespace FortCode.Data
{

    public enum DataAuditCategory
    {
        Other =1,
        Bar =2,
        Drink =3,
        City =4,
    }

    [DataContract]
    [DebuggerDisplay("AuditMessage = {AuditMessage}, Category = {Category}")]
    [Table("datauditlog")]
    public class DataAuditLog
    {

        [Required]
        [DataMember]
        [Column("message", Order = 0)]
        public string AuditMessage { get; set; }


        [Required]
        [DataMember]
        [Column("category", Order = 1)]
        public DataAuditCategory Category { get; set; }


        [Required]
        [DataMember]
        [Column("userid", Order = 2)]
        public string UserId { get; set; }

        [Required]
        [DataMember]
        [Column("auditdate", Order = 3)]
        public DateTime AuditDate { get; set; }
    }
}
