﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Reflection;
using Newtonsoft.Json;
using System.Text;

namespace FortCode.Data
{

    internal static class DataLoadUtlity
    {

        internal static IEnumerable<T> GetSeedDataContent<T>(string dataName)
        {
            string resourceName = $"FortCode.Data.Migrations.SeedData.{dataName}.json";

            string resourceContent = GetEmbeddedContent(resourceName);

            IEnumerable<T> retValue = JsonConvert.DeserializeObject<List<T>>(resourceContent);

            return retValue;
        }


        private static string GetEmbeddedContent(string resourceName)
        {
            string retContent = null;

            try
            {

                var assembly = Assembly.GetExecutingAssembly();


                var resourceStream = assembly.GetManifestResourceStream(resourceName);
                using (var reader = new StreamReader(resourceStream, Encoding.UTF8))
                {
                    retContent = reader.ReadToEnd();
                }
            }
            catch (Exception ex)
            {
                
                throw new Exception($"Error getting resource {resourceName}", ex);
            }



            return retContent;


        }

    }
}
