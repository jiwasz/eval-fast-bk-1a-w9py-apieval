﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics;
using System.Runtime.Serialization;
using System.Text;

namespace FortCode.Data
{

    [DataContract]
    [DebuggerDisplay("UserId = {UserId}, BarId = {BarId}")]
    [Table("userbarxref")]
    public class UserBarXRef
    {

        [Column("userid", Order = 0)]
        public string UserId { get; set; }

        [Column("barid", Order = 1)]
        public Guid BarId { get; set; }

        public DataBar Bar { get; set; }
    }
}
