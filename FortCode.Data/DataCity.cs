﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics;
using System.Runtime.Serialization;


namespace FortCode.Data
{

  //  [Index(nameof(CountryId), nameof(Name), IsUnique = true, Name = "idx_countryname")]
    [DataContract]
    [DebuggerDisplay("Id = {Id}, Name = {Name}")]
    [Table("cities")]
    public class DataCity
    {
        [DataMember]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        [System.ComponentModel.DataAnnotations.Key]
        [Column("id", Order = 0)]
        public Guid Id { get; set; }

        [Required]
        [Column("countryid", Order = 1)]
        public Guid CountryId { get; set; }

        
        [Required]
        [DataMember]
        [Column("name", Order = 2)]
        public string Name { get; set; }


        public DataCountry Country { get; set; }

        public IEnumerable<UserCityXRef> UserCities { get; set; } = new List<UserCityXRef>();
    }
}
