﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics;
using System.Runtime.Serialization;
using System.Text;

namespace FortCode.Data
{

    [DataContract]
    [DebuggerDisplay("Id = {Id}, Name = {Name}")]
    [Table("bars")]
    public class DataBar
    {
        [DataMember]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        [System.ComponentModel.DataAnnotations.Key]
        [Column("id", Order = 0)]
        public Guid Id { get; set; }


        [Required]
        [DataMember]
        [Column("name", Order = 1)]
        public string Name { get; set; }

        public IEnumerable<DataDrink> Drinks { get; set; } = new List<DataDrink>();

        public IEnumerable<UserBarXRef> UserBars { get; set; } = new List<UserBarXRef>();
    }
}
