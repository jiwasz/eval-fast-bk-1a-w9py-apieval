﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

[assembly: InternalsVisibleTo("FontCode.Repository.Tests")]

namespace FortCode.Data
{

    [ExcludeFromCodeCoverage]
    public class FortCodeDbContextFactory : IDesignTimeDbContextFactory<FortCodeDbContext>
    {
        private const string ENV_DBCONTEXT = "FORTCODEENV_ConnectionStrings__DbContext";

        public FortCodeDbContext CreateDbContext(string[] args)
        {
            string envSetting = Environment.GetEnvironmentVariable(ENV_DBCONTEXT);
            if(string.IsNullOrWhiteSpace(envSetting))
            {
                throw new Exception($"Connection string environment variabel {ENV_DBCONTEXT} not found");
            }


            return CreateDbContext(envSetting);
        }


        public FortCodeDbContext CreateDbContext(string connectionString)
        {
            var optionsBuilder = new DbContextOptionsBuilder<FortCodeDbContext>();

            optionsBuilder.EnableSensitiveDataLogging();

            optionsBuilder.UseSqlServer(connectionString);

            ILoggerFactory loggerFactory = LoggerFactory.Create(builder =>
            {
                builder.AddConsole();
            });

            ILogger<FortCodeDbContext> contextLogger = loggerFactory.CreateLogger<FortCodeDbContext>();


            FortCodeDbContext context = new FortCodeDbContext(optionsBuilder.Options, contextLogger);

            return context;

        }
    }
}
