﻿using FortCode.ModelsV1;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FortCode.RepositoryV1
{
    public interface IUserCityRepositoryV1
    {
        public Task<AddFavoriteCityResponseV1> AddUserFavoriteCityAsync(string cityName, Guid countryId, string userId);
    }
}
