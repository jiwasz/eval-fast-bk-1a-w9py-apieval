﻿using FortCode.Data;
using FortCode.ModelsV1;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FortCode.RepositoryV1
{
    public class CountryRepositoryV1 : ICountryRepositoryV1
    {
        private FortCodeDbContext _dbContext;

        public CountryRepositoryV1(FortCodeDbContext dbContext)
        {
            _dbContext = dbContext ?? throw new ArgumentNullException(nameof(dbContext));
        }

        public async Task<GetCountryResponseV1> GetCountryAsync(Guid id)
        {

            if (id == default(Guid))
                throw new ArgumentException("Country id must be set");

            DataCountry foundCountry = await _dbContext.Countries.FindAsync(id).ConfigureAwait(false);

            GetCountryResponseV1 retCountry = new GetCountryResponseV1();
            if (foundCountry != null)
                retCountry.Country = foundCountry.ToCountryV1();

            return retCountry;
        }

        public async Task<GetCountryResponseV1> GetCountryAsync(string name)
        {
            if (name == null)
                throw new ArgumentNullException(nameof(name));

            if (string.IsNullOrWhiteSpace(name))
                throw new ArgumentException("Country name cannot be empty");

            string searchName = name.Trim().ToLower();

            DataCountry foundCountry = await _dbContext.Countries.SingleOrDefaultAsync(x => x.Name.ToLower() == searchName)
                .ConfigureAwait(false);

            GetCountryResponseV1 retCountry = new GetCountryResponseV1();
            if (foundCountry != null)
                retCountry.Country = foundCountry.ToCountryV1();

            return retCountry;
        }
    }
}
