﻿using FortCode.Data;
using FortCode.ModelsV1;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FortCode.RepositoryV1
{
    public interface ICityRepositoryV1
    {
        public Task<GetCityResponseV1> GetCityAsync(Guid id);

        public Task<GetCityResponseV1> GetCityAsync(string name);
    }
}
