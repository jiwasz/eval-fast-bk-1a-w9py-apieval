﻿using FortCode.Data;
using FortCode.ModelsV1;
using System;
using System.Threading.Tasks;

namespace FortCode.RepositoryV1
{ 
    public interface ICountryRepositoryV1
    {
        public Task<GetCountryResponseV1> GetCountryAsync(Guid id);

        public Task<GetCountryResponseV1> GetCountryAsync(string name);
        
    }
}
