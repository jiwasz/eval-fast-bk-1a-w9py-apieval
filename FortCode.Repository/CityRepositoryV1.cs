﻿using FortCode.Data;
using FortCode.ModelsV1;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace FortCode.RepositoryV1
{
    public class CityRepositoryV1 : ICityRepositoryV1
    {

        private FortCodeDbContext _dbContext;

        public CityRepositoryV1(FortCodeDbContext dbContext)
        {
            _dbContext = dbContext ?? throw new ArgumentNullException(nameof(dbContext));
        }

        public Task<GetCityResponseV1> GetCityAsync(Guid id)
        {
            throw new NotImplementedException();
        }

        public Task<GetCityResponseV1> GetCityAsync(string name)
        {
            throw new NotImplementedException();
        }
    }
}
