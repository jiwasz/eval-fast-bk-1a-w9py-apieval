﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FortCode.Data;
using FortCode.ModelsV1;

namespace FortCode.RepositoryV1
{
    internal static class RepositoryExtentionsV1
    {

        internal static CountryV1 ToCountryV1(this DataCountry country)
        {
            CountryV1 retCountry = new CountryV1
            {
                Id = country.Id,
                Name = country.Name
            };

            return retCountry;
        }


        internal static CityV1 ToCityV1(this DataCity city)
        {
            CityV1 retCity = new CityV1
            {
                Id = city.Id,
                Name = city.Name
            };

            return retCity;
        }

    }
}
