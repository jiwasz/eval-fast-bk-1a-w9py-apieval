﻿using FortCode.Data;
using FortCode.ModelsV1;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FortCode.RepositoryV1
{
    public class UserCityRepositoryV1 : IUserCityRepositoryV1
    {
        private FortCodeDbContext _dbContext;

        public UserCityRepositoryV1(FortCodeDbContext dbContext)
        {
            _dbContext = dbContext ?? throw new ArgumentNullException(nameof(dbContext));
        }

        public async Task<AddFavoriteCityResponseV1> AddUserFavoriteCityAsync(string cityName, Guid countryId, string userId)
        {
            if (cityName == null)
                throw new ArgumentNullException(nameof(cityName));

            if (string.IsNullOrEmpty(cityName))
                throw new ArgumentException($"{nameof(cityName)} cannot be empty");

            if (countryId == default(Guid))
                throw new ArgumentException($"{nameof(countryId)} cannot be empty");

            if (userId == null)
                throw new ArgumentNullException(nameof(userId));

            if (string.IsNullOrEmpty(userId))
                throw new ArgumentException($"{nameof(userId)} cannot be empty");

            AddFavoriteCityResponseV1 retResponse = new AddFavoriteCityResponseV1();

            DataCity addedCity = null;

            string searchCity = cityName.Trim().ToLowerInvariant();


            using (var trans = await _dbContext.Database.BeginTransactionAsync().ConfigureAwait(false))
            {


                addedCity = _dbContext.Cities.SingleOrDefault(x => x.CountryId == countryId && x.Name.ToLower() == searchCity);


                UserCityXRef userFavorite = null;


                if (addedCity == null)
                {
                    addedCity = new DataCity
                    {
                        Id = Guid.NewGuid(),
                        CountryId = countryId,
                        Name = cityName.Trim()
                    };

                    _dbContext.Cities.Add(addedCity);
                }
                else
                {
                    // City already exists. It may already be marked as a favorite by the same user
                    userFavorite = await _dbContext.UserCities.FindAsync(userId, addedCity.Id);
                }

                if (userFavorite == null)
                {
                    userFavorite = new UserCityXRef();
                    userFavorite.CityId = addedCity.Id;
                    userFavorite.UserId = userId;

                    _dbContext.UserCities.Add(userFavorite);
                }


                await _dbContext.SaveChangesAsync();
                await trans.CommitAsync();

            }

            retResponse.City = addedCity.ToCityV1();

            return retResponse;
        }

    }
}
