﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FortCode.ModelsV1
{
    public class CityV1
    {
        public Guid Id { get; set; }

        public string Name { get; set; }
    }
}
