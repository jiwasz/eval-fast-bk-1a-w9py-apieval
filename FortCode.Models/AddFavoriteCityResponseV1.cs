﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FortCode.ModelsV1
{
    public class AddFavoriteCityResponseV1
    {

        public CityV1 City { get; set; }

    }
}
